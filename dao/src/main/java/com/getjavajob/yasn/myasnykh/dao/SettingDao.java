package com.getjavajob.yasn.myasnykh.dao;

import com.getjavajob.yasn.myasnykh.dto.Setting;
import com.getjavajob.yasn.myasnykh.dto.ProfileSetting;
import com.getjavajob.yasn.myasnykh.dto.SettingValue;

import java.util.List;

/**
 * Created by User on 13.09.2015.
 */
public interface SettingDao {
    boolean updateSecuritySettings(List<ProfileSetting> profileSettings);

    SettingValue searchSecuritySettingValueByDescription(String setting, String value);

    List<SettingValue> getAllSettingsValues();

    List<Setting> getAllSettings();
}
