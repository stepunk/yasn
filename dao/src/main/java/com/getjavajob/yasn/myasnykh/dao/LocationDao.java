package com.getjavajob.yasn.myasnykh.dao;

import com.getjavajob.yasn.myasnykh.dto.City;
import com.getjavajob.yasn.myasnykh.dto.Country;

import java.util.List;

/**
 * Created by User on 13.09.2015.
 */
public interface LocationDao {
    List<Country> getCounties();

    List<City> getCities(int countryId);
}
