package com.getjavajob.yasn.myasnykh.dao;

import com.getjavajob.yasn.myasnykh.dto.Message;
import com.getjavajob.yasn.myasnykh.dto.Profile;

import java.util.List;

/**
 * Created by test on 14.08.2015.
 */
public interface MessageDao {
    boolean addWallMessage(Message message);

    List<Message> getWallMessages(Profile profile);

    List<Message> getMessages(int firstUserId, int secondUserId);

    List<Profile> getInterlocutors(int userId);

    boolean updateMessage(List<Message> messages);

    List getNewMessages(int userId, int authorId);
}
