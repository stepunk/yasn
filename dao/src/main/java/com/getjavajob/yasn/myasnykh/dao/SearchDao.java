package com.getjavajob.yasn.myasnykh.dao;

import com.getjavajob.yasn.myasnykh.dto.Filter;
import com.getjavajob.yasn.myasnykh.dto.Profile;

import java.util.List;

/**
 * Created by User on 13.09.2015.
 */
public interface SearchDao {
    List<Profile> getUsers(Filter filter);
}
