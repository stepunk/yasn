package com.getjavajob.yasn.myasnykh.dao.Exceptions;

/**
 * Created by test on 11.08.2015.
 */
public class CityNotFoundException extends RuntimeException{
    public CityNotFoundException(String message) {
        super(message);
    }
}
