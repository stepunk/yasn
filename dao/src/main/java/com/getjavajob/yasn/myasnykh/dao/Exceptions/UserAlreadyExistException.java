package com.getjavajob.yasn.myasnykh.dao.Exceptions;

/**
 * Created by test on 11.08.2015.
 */
public class UserAlreadyExistException extends RuntimeException{
    public UserAlreadyExistException(String message) {
        super(message);
    }
}
