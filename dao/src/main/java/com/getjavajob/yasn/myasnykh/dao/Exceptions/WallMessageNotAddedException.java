package com.getjavajob.yasn.myasnykh.dao.Exceptions;

/**
 * Created by User on 14.08.2015.
 */
public class WallMessageNotAddedException extends RuntimeException {
    public WallMessageNotAddedException(String message) {
        super(message);
    }
}
