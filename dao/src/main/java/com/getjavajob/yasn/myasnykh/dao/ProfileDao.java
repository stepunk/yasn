package com.getjavajob.yasn.myasnykh.dao;

import com.getjavajob.yasn.myasnykh.dto.Image;
import com.getjavajob.yasn.myasnykh.dto.Profile;

/**
 * Created by User on 30.07.2015.
 */
public interface ProfileDao {
    Integer getProfileIdByEmail(String email);

    Profile saveProfile(Profile user);

    Profile getProfile(int userId);

    Profile updateProfile(Profile user);

    boolean checkIfUsersFriends(int ownerId, int viewerId);

    boolean storeImage(Image image);

    Image getImage(int userId);

//    String getHealth();

}
