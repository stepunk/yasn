package com.getjavajob.yasn.myasnykh.dao;

import com.getjavajob.yasn.myasnykh.dto.Message;
import com.getjavajob.yasn.myasnykh.dto.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;


/**
 * Created by test on 14.08.2015.
 */
@Service
public class MessageDaoImpl implements MessageDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public boolean addWallMessage(Message message) {
        entityManager.merge(message);
        return true;
    }

    @Override
    public List<Message> getWallMessages(Profile profile) {
        final Query query = entityManager.createQuery("" +
                "select m from Message m " +
                "where m.userId =(:userId) " +
                "order by m.date desc ");
        query.setParameter("userId", profile);
        List<Message> messages = query.getResultList();
        return messages;
    }

    public List<Profile> getInterlocutors(int userId) {
        final Query query = entityManager.createQuery("" +
                "select distinct m.authorId from Message m " +
                "where m.isPrivate=true " +
                "and m.userId.userId=:userId " +
                "UNION " +
                "select distinct n.userId from Message n " +
                "where n.isPrivate=true " +
                "and n.authorId.userId=:userId");
        query.setParameter("userId", userId);
        List<Profile> interlocutors = query.getResultList();

        return interlocutors;
    }

    public List<Message> getDialogs(int userId) {
        final Query query = entityManager.createQuery("" +
                "select distinct m from Message m " +
                "where m.messageId in (" +
                "select distinct  b.messageId " +
                "from Message as b " +
                "where b.authorId=m.authorId " +
                "and b.isPrivate=true) " +
                "and (m.userid=:userId or m.authorId=:userId)" +
                "order by m.datemessage DESC, m.authorId ");
        query.setParameter("userId", userId);
        List<Message> messages = query.getResultList();
        return messages;
    }

    public List<Message> getMessages(int firstUserId, int secondUserId) {
        final Query query = entityManager.createQuery("" +
                "select distinct m from Message m " +
                "where (m.authorId.userId=:firstUserId and m.userId.userId = :secondUserId) " +
                "or (m.userId.userId=:firstUserId and   m.authorId.userId=:secondUserId)" +
                "order by m.date" +
                "");
        query.setParameter("firstUserId", firstUserId);
        query.setParameter("secondUserId", secondUserId);
        List<Message> messages = query.getResultList();
//        Collections.reverse(messages);
        return messages;
    }

    @Override
    @Transactional
    public boolean updateMessage(List<Message> messages) {
        try {
            for (Message message : messages) {
                entityManager.merge(message);
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public List<Message> getNewMessages(int userId, int authorId) {
        Query query = entityManager.createQuery("" +
                "select m from Message m " +
                "where m.userId.userId=:userId " +
                "and m.authorId.userId =:authorId " +
                "and m.isRead=false " +
                "order by m.date");
        query.setParameter("userId", userId);
        query.setParameter("authorId", authorId);
        List<Message> resultList = query.getResultList();
        return resultList;
    }
}

