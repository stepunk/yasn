package com.getjavajob.yasn.myasnykh.dao;

import com.getjavajob.yasn.myasnykh.dto.City;
import com.getjavajob.yasn.myasnykh.dto.Country;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by User on 13.09.2015.
 */
@Service
public class LocationDaoImpl implements LocationDao {
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<Country> getCounties() {
        final Query query = entityManager.createQuery("select c from Country c");
        return query.getResultList();
    }

    @Override
    public List<City> getCities(int countryId) {
        Query query = entityManager.createQuery("select c from City c " +
                "where c.country.countryId =:countryId");
        query.setParameter("countryId", countryId);
        return query.getResultList();
    }

//    @Override
//    public Country getCountry(String countryName) {
//        final Query query = entityManager.createQuery("" +
//                "select c from Country c " +
//                "where c.country=(:country) ");
//        query.setParameter("country", countryName);
//        List<Country> country = query.getResultList();
//        return country.get(0);
//    }
//
//    @Override
//    public City getCity(String cityName, Country country) {
//        final Query query = entityManager.createQuery("" +
//                "select c from City c " +
//                "where c.city=:city " +
//                "and c.country=:country");
//        query.setParameter("city", cityName);
//        query.setParameter("country", country);
//        List<City> city = query.getResultList();
//        return city.get(0);
//    }

}
