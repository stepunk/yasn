package com.getjavajob.yasn.myasnykh.dao;

import com.getjavajob.yasn.myasnykh.dao.Exceptions.UserAlreadyExistException;
import com.getjavajob.yasn.myasnykh.dto.Image;
import com.getjavajob.yasn.myasnykh.dto.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by User on 28.07.2015.
 */
@Service
public class ProfileDaoImpl implements ProfileDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Integer getProfileIdByEmail(String email) {
        final Query query = entityManager.createQuery("" +
                "select p from Profile p " +
                "where p.email =(:email) ");
        query.setParameter("email", email);
        final List<Profile> profile = query.getResultList();
        if (profile.size() == 0) {
            return null;
        }
        return profile.get(0).getUserId();
    }

    @Override
    public Profile getProfile(int userId) {
        final Profile profile = entityManager.find(Profile.class, userId);

        return profile;
    }

    @Transactional
    @Override
    public Profile saveProfile(Profile user) {
        if (checkIfUserExist(user.getEmail())) {
            throw new UserAlreadyExistException("User already exist");
        }
        entityManager.persist(user);
        return user;
    }

    @Override
    @Transactional
    public Profile updateProfile(Profile profile) {
        entityManager.merge(profile);
        return profile;
    }

    @Override
    public boolean checkIfUsersFriends(int ownerId, int viewerId) {
        //todo correct it
        return true;
    }

    //toDo
//    @Override
//    public String getHealth() {
//        try (Connection connection = dataSource.getConnection()) {
//            Statement statement = connection.createStatement();
//            ResultSet result = statement.executeQuery(
//                    "SELECT COUNT(*) FROM Profile;");
//            while (result.next()) {
//                return "user records:" + result.getInt("count");
//            }
//        } catch (SQLException e) {
//
//            return "incorrect log in" + e.toString();
//        }
//        return "incorrect log in";
//    }

    protected boolean checkIfUserExist(String email) {
        Query query = entityManager.createQuery("select p from Profile p " +
                "where p.email=:email");
        query.setParameter("email", email);
        List<Profile> profile = query.getResultList();

        return profile.size() == 1;
    }

    @Transactional
    public boolean storeImage(Image image) {
        entityManager.merge(image);
        return true;
    }

    @Override
    public Image getImage(int userId) {
        final Query query = entityManager.createQuery("" +
                "select c from Image c " +
                "where c.userId=(:userId) ");
        query.setParameter("userId", userId);
        List<Image> image = query.getResultList();
        if (image.size() == 0) {
            return null;
        }
        return image.get(0);
    }
}
