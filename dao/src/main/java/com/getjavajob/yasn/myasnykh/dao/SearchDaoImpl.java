package com.getjavajob.yasn.myasnykh.dao;

import com.getjavajob.yasn.myasnykh.dto.Filter;
import com.getjavajob.yasn.myasnykh.dto.City;
import com.getjavajob.yasn.myasnykh.dto.Country;
import com.getjavajob.yasn.myasnykh.dto.Profile;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by User on 13.09.2015.
 */
@Service
public class SearchDaoImpl implements SearchDao {
    @PersistenceContext
    private EntityManager entityManager;

    public List<Profile> getUsers(Filter filter) {
        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Profile> query = criteriaBuilder.createQuery(Profile.class);
        final Root<Profile> profileRoot = query.from(Profile.class);
        CriteriaQuery select = query.select(profileRoot);


        List<Predicate> predicatesOption = new ArrayList<>();
        if (filter.getFirstName() != null || filter.getLastName() != null) {
            Predicate nameAndLastNamePredicate = getNameAndLastNamePredicate(filter, criteriaBuilder, profileRoot);
            predicatesOption.add(nameAndLastNamePredicate);
        }
        if (filter.getAgeFrom() != null) {
            Integer ageTo = filter.getAgeTo();
            if (ageTo==null){
                ageTo=120;
            }
            Date from = getBirthDateFrom(ageTo);
            Date to = getBirthDateTo(filter.getAgeFrom());
            Predicate predicate = criteriaBuilder.between(profileRoot.<Date>get("birthDay"), from, to);
            predicatesOption.add(predicate);
        }
        if (filter.getGender() != null) {
            Predicate predicate = criteriaBuilder.like(profileRoot.<String>get("gender"), filter.getGender());
            predicatesOption.add(predicate);
        }
        if (filter.getCountryId() != null) {
            Predicate predicate = criteriaBuilder.equal(profileRoot.<Country>get("country").<Integer>get("countryId"), filter.getCountryId());
            predicatesOption.add(predicate);
        }
        if (filter.getCity() != null) {
            Predicate predicate = criteriaBuilder.like(profileRoot.<City>get("city").<String>get("city"), filter.getCity());
            predicatesOption.add(predicate);
        }

        Predicate optionPredicate = criteriaBuilder.and(predicatesOption.toArray(new Predicate[predicatesOption.size()]));

        query.where(optionPredicate);

        final TypedQuery<Profile> executableQuery = entityManager.createQuery(select);
        final List<Profile> profiles = executableQuery.getResultList();
        return profiles;
    }

    private Predicate getNameAndLastNamePredicate(Filter filter, CriteriaBuilder criteriaBuilder, Root<Profile> profileRoot) {

        List<Predicate> predicatesName1 = new ArrayList<>();
        List<Predicate> predicatesName2 = new ArrayList<>();

        if (filter.getFirstName() != null && filter.getFirstName().length() > 0) {
            Expression<String> literal = criteriaBuilder.upper(criteriaBuilder.literal(filter.getFirstName() + "%"));
            Predicate predicate = criteriaBuilder.like(criteriaBuilder.upper(profileRoot.<String>get("firstName")), literal);
            predicatesName1.add(predicate);
        }
        if (filter.getLastName() != null && filter.getLastName().length() > 0) {
            Expression<String> literal = criteriaBuilder.upper(criteriaBuilder.literal(filter.getLastName() + "%"));
            Predicate predicate = criteriaBuilder.like(criteriaBuilder.upper(profileRoot.<String>get("lastName")), literal);
            predicatesName1.add(predicate);
        }

        if (filter.getFirstName() != null && filter.getFirstName().length() > 0) {
            Expression<String> literal = criteriaBuilder.upper(criteriaBuilder.literal(filter.getFirstName() + "%"));
            Predicate predicate = criteriaBuilder.like(criteriaBuilder.upper(profileRoot.<String>get("lastName")), literal);
            predicatesName2.add(predicate);
        }
        if (filter.getLastName() != null && filter.getLastName().length() > 0) {
            Expression<String> literal = criteriaBuilder.upper(criteriaBuilder.literal(filter.getLastName() + "%"));
            Predicate predicate = criteriaBuilder.like(criteriaBuilder.upper(profileRoot.<String>get("firstName")), literal);
            predicatesName2.add(predicate);
        }

        Predicate name1 = criteriaBuilder.and(predicatesName1.toArray(new Predicate[predicatesName1.size()]));
        Predicate name2 = criteriaBuilder.and(predicatesName2.toArray(new Predicate[predicatesName2.size()]));
        Predicate name = criteriaBuilder.or(name1, name2);

        return name;
    }

    private Date getBirthDateFrom(int age) {
        Date today = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);

        calendar.add(Calendar.YEAR, -(age + 1));
        return calendar.getTime();
    }
    private Date getBirthDateTo(int age) {
        Date today = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);

        calendar.add(Calendar.YEAR, -(age));
        calendar.add(Calendar.DAY_OF_YEAR,-1);
        return calendar.getTime();
    }
}
