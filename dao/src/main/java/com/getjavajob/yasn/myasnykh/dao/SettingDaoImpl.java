package com.getjavajob.yasn.myasnykh.dao;

import com.getjavajob.yasn.myasnykh.dto.ProfileSetting;
import com.getjavajob.yasn.myasnykh.dto.Setting;
import com.getjavajob.yasn.myasnykh.dto.SettingValue;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by User on 13.09.2015.
 */
@Service
public class SettingDaoImpl implements SettingDao{
    @PersistenceContext
    EntityManager entityManager;
    @Override
    public List<SettingValue> getAllSettingsValues() {
        Query query = entityManager.createQuery("select s from SettingValue s");
        List<SettingValue> resultList = query.getResultList();
        return resultList;
    }

    @Override
    @Transactional
    public boolean updateSecuritySettings(List<ProfileSetting> profileSettings) {
        for (ProfileSetting profileSetting : profileSettings) {
            entityManager.merge(profileSetting);
        }
        return true;
    }

    @Override
    public SettingValue searchSecuritySettingValueByDescription(String setting, String value) {
        final Query query = entityManager.createQuery("" +
                "select v from SettingValue v " +
                "where v.setting.description =(:setting) " +
                "and v.value= (:settingValue)");
        query.setParameter("setting", setting);
        query.setParameter("settingValue", value);
        final List<SettingValue> profile = query.getResultList();
        return profile.get(0);
    }

    @Override
    public List<Setting> getAllSettings() {
        Query query = entityManager.createQuery("select s from Setting s");
        List<Setting> resultList = query.getResultList();
        return resultList;
    }
}
