package com.getjavajob.yasn.myasnykh.dao.Exceptions;

/**
 * Created by test on 11.08.2015.
 */
public class CountryNotFoundException extends RuntimeException{
    public CountryNotFoundException(String message) {
        super(message);
    }
}
