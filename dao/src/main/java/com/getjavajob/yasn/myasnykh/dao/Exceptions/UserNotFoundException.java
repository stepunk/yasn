package com.getjavajob.yasn.myasnykh.dao.Exceptions;

/**
 * Created by User on 14.08.2015.
 */
public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(String message) {
        super(message);
    }
}
