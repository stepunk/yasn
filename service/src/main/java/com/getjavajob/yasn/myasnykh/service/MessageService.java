package com.getjavajob.yasn.myasnykh.service;


import com.getjavajob.yasn.myasnykh.dto.Message;
import com.getjavajob.yasn.myasnykh.dto.Profile;

import java.util.List;

/**
 * Created by User on 14.08.2015.
 */
public interface MessageService {
    List<Message> getWallMessages(Profile profile);

    boolean addWallMessage(Message message);

    List<Message> getMessages(int firstUserId, int secondUserId);

    boolean updateMessage(List<Message> messages);

    List<Message> getNewMessages(int userId, int authorId);

    List<Profile> getInterlocutors(int userId);
}
