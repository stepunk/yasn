package com.getjavajob.yasn.myasnykh.service;

import com.getjavajob.yasn.myasnykh.dto.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    ProfileService profileService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Integer userId = profileService.getProfileIdByEmail(email);
        Profile profile = profileService.getProfile(userId);
        return new User(profile.getUserId().toString(), profile.getPassword(), getGrantedAuthorities(profile));
    }

    private List<GrantedAuthority> getGrantedAuthorities(Profile profile) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        return authorities;
    }
}
