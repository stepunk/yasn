package com.getjavajob.yasn.myasnykh.service;

import com.getjavajob.yasn.myasnykh.dao.MessageDao;
import com.getjavajob.yasn.myasnykh.dto.Message;
import com.getjavajob.yasn.myasnykh.dto.Profile;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by test on 14.08.2015.
 */
public class MessageServiceImpl implements MessageService {

    private final MessageDao dao;

    @Autowired
    private MessageServiceImpl(MessageDao messageDao) {
        this.dao = messageDao;
    }

    public List<Message> getWallMessages(Profile profile) {

        List<Message> messages = dao.getWallMessages(profile);

        return messages;
    }

    public boolean addWallMessage(Message message) {
        return dao.addWallMessage(message);
    }

    public List<Message> getMessages(int firstUserId, int secondUserId) {
        return dao.getMessages(firstUserId, secondUserId);
    }

    public boolean updateMessage(List<Message> messages) {
        return dao.updateMessage(messages);
    }

    public List<Message> getNewMessages(int userId, int authorId){
        return dao.getNewMessages(userId,authorId);
    }

    public List<Profile> getInterlocutors(int userId) {
        return dao.getInterlocutors(userId);
    }
}
