package com.getjavajob.yasn.myasnykh.service;

import com.getjavajob.yasn.myasnykh.dao.LocationDao;
import com.getjavajob.yasn.myasnykh.dao.ProfileDao;
import com.getjavajob.yasn.myasnykh.dao.SearchDao;
import com.getjavajob.yasn.myasnykh.dao.SettingDao;
import com.getjavajob.yasn.myasnykh.dto.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ProfileServiceImpl implements ProfileService {
    private ProfileDao profileDao;
    private SearchDao searchDao;
    private SettingDao settingDao;
    private LocationDao locationDao;

    @Autowired
    private ProfileServiceImpl(ProfileDao profileDao, SearchDao searchDao, SettingDao settingDao, LocationDao locationDao) {
        this.profileDao = profileDao;
        this.searchDao = searchDao;
        this.settingDao = settingDao;
        this.locationDao = locationDao;
    }

    @Override
    public Profile getProfile(int userId) {
        return profileDao.getProfile(userId);
    }

    @Override
    public Profile saveProfile(Profile user) {
        return profileDao.saveProfile(user);
    }

    @Override
    public Integer getProfileIdByEmail(String email) {
        return profileDao.getProfileIdByEmail(email);
    }

    @Override
    public Profile updateProfile(Profile profile) {
        return profileDao.updateProfile(profile);
    }

    @Override
    public List<Profile> getUsers(Filter filter) {
        return searchDao.getUsers(filter);
    }

//    public Country getCountry(String countryName) {
//        return profileDao.getCountry(countryName);
//    }

    @Override
    public List<Country> getCountries() {
        return locationDao.getCounties();
    }

    @Override
    public List<City> getCities(int countryId) {
        return locationDao.getCities(countryId);
    }

//    public City getCity(String cityName, Country country) {
//        return profileDao.getCity(cityName, country);
//    }

    @Override
    public boolean updateSecuritySettings(List<ProfileSetting> profileSettings) {
        return settingDao.updateSecuritySettings(profileSettings);
    }

    @Override
    public SettingValue searchSecuritySettingValueByDescription(String setting, String value) {
        return settingDao.searchSecuritySettingValueByDescription(setting, value);
    }

    @Override
    public List<Setting> getAllSettings() {
        return settingDao.getAllSettings();
    }

    @Override
    public List<SettingValue> getAllSettingsValues() {
        return settingDao.getAllSettingsValues();
    }

    public boolean checkIfUsersFriends(int ownerId, int viewerId) {
        return profileDao.checkIfUsersFriends(ownerId, viewerId);
    }

    @Override
    public boolean storeImage(Image image) {
        return profileDao.storeImage(image);
    }

    @Override
    public Image getImage(int userId) {
        return profileDao.getImage(userId);
    }

//    @Override
//    public String getHealth() {
//        return profileDao.getHealth();
//    }
}
