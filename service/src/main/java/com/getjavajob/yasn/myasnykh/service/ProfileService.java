package com.getjavajob.yasn.myasnykh.service;


import com.getjavajob.yasn.myasnykh.dto.*;

import java.util.List;


public interface ProfileService {

    Profile getProfile(int userId);

    Profile saveProfile(Profile user);

    Integer getProfileIdByEmail(String email);

    boolean updateSecuritySettings(List<ProfileSetting> profileSettings);

    SettingValue searchSecuritySettingValueByDescription(String setting, String value);

    List<Setting> getAllSettings();

    List<SettingValue> getAllSettingsValues();

    Profile updateProfile(Profile profile);

    List<Profile> getUsers(Filter filter);

    List<Country> getCountries();

    List<City> getCities(int countryId);

    boolean checkIfUsersFriends(int ownerId, int viewerId);

    boolean storeImage(Image image);

    Image getImage(int userId);

//    String getHealth();
//
//    Country getCountry(String countryName);
//
//    City getCity(String cityName, Country country);

}
