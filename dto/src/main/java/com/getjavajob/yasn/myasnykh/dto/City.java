package com.getjavajob.yasn.myasnykh.dto;

import javax.persistence.*;

/**
 * Created by User on 05.09.2015.
 */
@Entity
public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer cityId;

    @ManyToOne
    @JoinColumn(name = "countryid")
    private Country country;

    private String city;

//    @Override
//    public String toString() {
//        return city+ " cityId:"+cityId;
//    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        City city = (City) o;

        return !(cityId != null ? !cityId.equals(city.cityId) : city.cityId != null);

    }

    @Override
    public int hashCode() {
        return cityId != null ? cityId.hashCode() : 0;
    }
}
