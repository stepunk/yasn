package com.getjavajob.yasn.myasnykh.dto;

import javax.persistence.*;

/**
 * Created by User on 06.09.2015.
 */
@Entity
@Table(name = "UsersSettings")
public class ProfileSetting {
    public ProfileSetting() {
        key = new SettingKey();
    }

    @EmbeddedId
    private SettingKey key;

    @ManyToOne
    @JoinColumn(name = "value")
    private SettingValue value;

    public Setting getSetting() {
        return key.getSetting();
    }

    public void setSetting(Setting setting) {
        key.setSetting(setting);
    }

    public SettingValue getValue() {
        return value;
    }

    public void setValue(SettingValue value) {
        this.value = value;
    }

    public SettingKey getKey() {
        return key;
    }

    public void setKey(SettingKey key) {
        this.key = key;
    }
}
