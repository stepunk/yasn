package com.getjavajob.yasn.myasnykh.dto;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by User on 08.08.2015.
 */
@Entity
@Table(name = "Message")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int messageId;

    @NotNull
    private String text;

    @ManyToOne
    @JoinColumn(name = "userId")
    private Profile userId;

    @ManyToOne
    @JoinColumn(name = "authorId")
    private Profile authorId;

    @Column(name = "DateMessage")
    private Date date;
    private boolean isRead;
    private boolean isPrivate;

    @Override
    public String toString() {
        return "from: " + authorId + " to: " + userId + " data:" + date;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Profile getUserId() {
        return userId;
    }

    public void setUserId(Profile userId) {
        this.userId = userId;
    }

    public Profile getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Profile authorId) {
        this.authorId = authorId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        this.isRead = read;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }
}