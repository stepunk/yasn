package com.getjavajob.yasn.myasnykh.dto;

import javax.persistence.*;

/**
 * Created by User on 06.09.2015.
 */
@Entity
@Table(name = "SettingsValue")
public class SettingValue {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer valueId;

    @ManyToOne
    @JoinColumn(name = "Setting")
    private Setting setting;

    private String value;

    public Integer getValueId() {
        return valueId;
    }

    public void setValueId(Integer valueId) {
        this.valueId = valueId;
    }

    public Setting getSetting() {
        return setting;
    }

    public void setSetting(Setting setting) {
        this.setting = setting;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SettingValue that = (SettingValue) o;

        return valueId.equals(that.valueId);

    }

    @Override
    public int hashCode() {
        return valueId.hashCode();
    }
}
