package com.getjavajob.yasn.myasnykh.dto;

/**
 * Created by User on 01.08.2015.
 */
public class Error {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
