package com.getjavajob.yasn.myasnykh.dto;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by User on 18.09.2015.
 */
@Entity
@Table(name = "Image")
public class Image {
    @Id
    private Integer userId;

    private byte[] image;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
