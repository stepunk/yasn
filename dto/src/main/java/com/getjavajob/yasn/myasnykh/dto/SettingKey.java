package com.getjavajob.yasn.myasnykh.dto;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by User on 07.09.2015.
 */
@Embeddable
public class SettingKey implements Serializable {

    @Column(name = "UserId", nullable = false)
    private int userId;

    @ManyToOne
    @JoinColumn(name = "Setting")
    private Setting setting;

    public Setting getSetting() {
        return setting;
    }

    public void setSetting(Setting setting) {
        this.setting = setting;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SettingKey key = (SettingKey) o;

        if (userId != key.userId) return false;
        return !(setting != null ? !setting.equals(key.setting) : key.setting != null);

    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (setting != null ? setting.hashCode() : 0);
        return result;
    }
}
