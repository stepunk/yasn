/*****************************************************************************/
/* Script exports Transact SME Sberbank tables as "insert" statements        */
/*                                                                           */
/* Commandline arguments:                                                    */
/* <DataSource> <UserId> <Password> <AppNo> [<filenamePrefix> <depersonalisation yes/no>]
/*                                                                           */
/* Created by Vladislav Dzyubov for Sberbank                                 */
/* Modified by Denis Kravchuk, Anton Kabaev and Vladimir Kudinov             */
/*****************************************************************************/
// BinaryFileReader.js
// ------------------------------------------------------------------
//
// give the ability to read a binary file into an array of bytes,
// to Javascript.
//
// the mapping is based on code from:
//   http://www.codeproject.com/KB/scripting/Exsead7.aspx
//
// Created    : Fri May 28 05:20:31 2010
// Last-saved : <2010-May-28 06:01:34>
//
// ------------------------------------------------------------------

(function(){

    BinaryFileReader = {};

    var FileReadTypes = {
        adTypeBinary : 1,
        adTypeText   : 2
    };

    var backward = [];
    backward['C7']   = '80';
    backward['FC']   = '81';
    backward['E9']   = '82';
    backward['E2']   = '83';
    backward['E4']   = '84';
    backward['E0']   = '85';
    backward['E5']   = '86';
    backward['E7']   = '87';
    backward['EA']   = '88';
    backward['EB']   = '89';
    backward['E8']   = '8A';
    backward['EF']   = '8B';
    backward['EE']   = '8C';
    backward['EC']   = '8D';
    backward['C4']   = '8E';
    backward['C5']   = '8F';
    backward['C9']   = '90';
    backward['E6']   = '91';
    backward['C6']   = '92';
    backward['F4']   = '93';
    backward['F6']   = '94';
    backward['F2']   = '95';
    backward['FB']   = '96';
    backward['F9']   = '97';
    backward['FF']   = '98';
    backward['D6']   = '99';
    backward['DC']   = '9A';
    backward['A2']   = '9B';
    backward['A3']   = '9C';
    backward['A5']   = '9D';
    backward['20A7'] = '9E';
    backward['192']  = '9F';
    backward['E1']   = 'A0';
    backward['ED']   = 'A1';
    backward['F3']   = 'A2';
    backward['FA']   = 'A3';
    backward['F1']   = 'A4';
    backward['D1']   = 'A5';
    backward['AA']   = 'A6';
    backward['BA']   = 'A7';
    backward['BF']   = 'A8';
    backward['2310'] = 'A9';
    backward['AC']   = 'AA';
    backward['BD']   = 'AB';
    backward['BC']   = 'AC';
    backward['A1']   = 'AD';
    backward['AB']   = 'AE';
    backward['BB']   = 'AF';
    backward['2591'] = 'B0';
    backward['2592'] = 'B1';
    backward['2593'] = 'B2';
    backward['2502'] = 'B3';
    backward['2524'] = 'B4';
    backward['2561'] = 'B5';
    backward['2562'] = 'B6';
    backward['2556'] = 'B7';
    backward['2555'] = 'B8';
    backward['2563'] = 'B9';
    backward['2551'] = 'BA';
    backward['2557'] = 'BB';
    backward['255D'] = 'BC';
    backward['255C'] = 'BD';
    backward['255B'] = 'BE';
    backward['2510'] = 'BF';
    backward['2514'] = 'C0';
    backward['2534'] = 'C1';
    backward['252C'] = 'C2';
    backward['251C'] = 'C3';
    backward['2500'] = 'C4';
    backward['253C'] = 'C5';
    backward['255E'] = 'C6';
    backward['255F'] = 'C7';
    backward['255A'] = 'C8';
    backward['2554'] = 'C9';
    backward['2569'] = 'CA';
    backward['2566'] = 'CB';
    backward['2560'] = 'CC';
    backward['2550'] = 'CD';
    backward['256C'] = 'CE';
    backward['2567'] = 'CF';
    backward['2568'] = 'D0';
    backward['2564'] = 'D1';
    backward['2565'] = 'D2';
    backward['2559'] = 'D3';
    backward['2558'] = 'D4';
    backward['2552'] = 'D5';
    backward['2553'] = 'D6';
    backward['256B'] = 'D7';
    backward['256A'] = 'D8';
    backward['2518'] = 'D9';
    backward['250C'] = 'DA';
    backward['2588'] = 'DB';
    backward['2584'] = 'DC';
    backward['258C'] = 'DD';
    backward['2590'] = 'DE';
    backward['2580'] = 'DF';
    backward['3B1']  = 'E0';
    backward['DF']   = 'E1';
    backward['393']  = 'E2';
    backward['3C0']  = 'E3';
    backward['3A3']  = 'E4';
    backward['3C3']  = 'E5';
    backward['B5']   = 'E6';
    backward['3C4']  = 'E7';
    backward['3A6']  = 'E8';
    backward['398']  = 'E9';
    backward['3A9']  = 'EA';
    backward['3B4']  = 'EB';
    backward['221E'] = 'EC';
    backward['3C6']  = 'ED';
    backward['3B5']  = 'EE';
    backward['2229'] = 'EF';
    backward['2261'] = 'F0';
    backward['B1']   = 'F1';
    backward['2265'] = 'F2';
    backward['2264'] = 'F3';
    backward['2320'] = 'F4';
    backward['2321'] = 'F5';
    backward['F7']   = 'F6';
    backward['2248'] = 'F7';
    backward['B0']   = 'F8';
    backward['2219'] = 'F9';
    backward['B7']   = 'FA';
    backward['221A'] = 'FB';
    backward['207F'] = 'FC';
    backward['B2']   = 'FD';
    backward['25A0'] = 'FE';
    backward['A0']   = 'FF';

    var hD="0123456789ABCDEF";

    var d2h = function(d)
    {
        var h = hD.substr(d&15,1);
        while(d>15) {d>>>=4;h=hD.substr(d&15,1)+h;}
        return h;
    }

    var h2d = function(h)
    {
        return parseInt(h,16);
    }

    var toByteArray = function(inString) {
        var encArray = [];
        var sL = inString.length;
        for (var i=0;i<sL;i++) {
            var cc = inString.charCodeAt(i);
            if(cc>=128) {
                var h = backward[''+d2h(cc)];
                cc = h2d(h);
            }
            encArray.push(fixLeadingZero(cc.toString(16)));
        }
        return encArray.join('');
    }
    
    var fixLeadingZero = function(inString) {
        return (inString.length == 1) ? "0"+inString : inString;
    }


    var _internalReadAll = function(path) {
        var bs = WScript.CreateObject("ADODB.Stream")
        bs.Type = FileReadTypes.adTypeText;
        bs.CharSet = '437';
        bs.Open();
        bs.LoadFromFile(path);
        var what = bs.ReadText;
        bs.Close();
        return what;
    }

    BinaryFileReader.ReadAllBytesFromFile = function(name)
    {
        var string = _internalReadAll(name);
        return BinaryFileReader.ReadAllBytesFromString(string);
    }
    BinaryFileReader.ReadAllBytesFromString = function(str)
    {
        return toByteArray(str);
    }

})();


    var AppNo;
    var ConnectionString; // calculated from command line arguments

    var NeedDepersonalise = true; //put false only when moving data within bank - 3rd argument in command line
    //Note - OutFileName may be overidden by command line
    var OutFileName = "2change.sql";
    var Schema = "SME";
    var SysRecordkey = 0;
    var Tables = new Array("APPLICANTS", "APPLICANTS2", "APPLICANTS3", "DECISIONS", "EXPRESSCREDIT", "HUNTERVERIFICATION", "INTEGRATION", "INTEGRATION2", "INTEGRATION3", "INTEGRATION4", "LEGAL_GRNT",
        "LEGAL_GRNT2","LEGAL2", "MA_VISIT", "NP_GRNT", "NP_GRNT2", "NPG_HISTORY", "NPG_HISTORY2", "NPG_HISTORY_SYS", "PRODCAT", 
        "SB_SYS", "SELLER", "SELLER2", "SM_FIELDS", "SM_FIELDS2", "SM_FIELDS3", "TELVERIFICATIONINFO","ADD_GRNT","ADD_GRNT2","ADD_GRNT_HISTORY","ADD_GRNT_HISTORY2","ADD_GRNT_HISTORY_SYS",
        "SM_FIELDS4"
    );
    /*var BRSTables = new Array("BRS_ADMCOMPANY","BRS_AUTHFREENP","BRS_CERTINFO","BRS_CERTINFOIE","BRS_CHARTERCAPITAL","BRS_CITIZENSHIP","BRS_CMIFREGINFO","BRS_CMIFREGINFOIE","BRS_ECONACTIVITYTYPESINFO",
        "BRS_ECONACTIVITYTYPESINFOIE","BRS_EXTCALLS","BRS_FOREIGNFOUNDERMEMBER","BRS_IDENTDATA","BRS_IEMAIN","BRS_INFOABOUTCCD","BRS_JSCCAPITALACCOUNT","BRS_LICINFO","BRS_LICINFOIE","BRS_LLCCHARTERCAPITALPARTS",
        "BRS_LPADRESS","BRS_LPBRANCHINFO","BRS_LPESTABL","BRS_LPMAIN","BRS_LPMUNICIPALFOUNDERMEMBER","BRS_LPPIFINFO","BRS_LPREORGPREDECESSOR","BRS_LPREORGSTARTINFO","BRS_LPREORGSUCCESSOR","BRS_LPRFFOUNDERMEMBER",
        "BRS_LPSUBJECTRFFOUNDERMEMBER","BRS_LPTERMINTATION","BRS_NPFOUNDERMEMBER","BRS_NPMUNICIPALFOUNDERMEMBER","BRS_NPPIFINFO","BRS_NPRFFOUNDERMEMBER","BRS_NPSUBJECTRFFOUNDERMEMBER","BRS_PFREGINFO","BRS_PFREGINFOIE",
        "BRS_PROOFMINORSDOC","BRS_REGINFO","BRS_REGINFOBEFORE01012004","BRS_REPRESSINFO","BRS_REQUESTIEINFOTITLE","BRS_REQUESTLPINFOTITLE","BRS_RUSFOUNDERMEMBER","BRS_SHARESREGHOLDER","BRS_SIFREGINFO","BRS_SIFREGINFOIE",
        "BRS_STATEOFLP","BRS_TAXREGINFO","BRS_TAXREGINFOIE","BRS_USRIEINFOBYDOCS","BRS_USRLENTRIESINFO"
    );*/
    var PledgeTables = new Array("SME_AUTO","SME_EQUIPMENT");
    
    var DoDelete = true;
    var KeyIncrement;
    var IgnoreFields = new Array("T_");
	var NeedBlob = false;
    // Regular expressions can be used here
    var DePersonalise = new Array("T_\\d+_\\w+NAME", "T_\\d+_\\w+PASSPORT\\w+", "T_\\d+_\\w+PHONE",
        "T_\\d+_\\w+STREET", "T_\\d+_\\w+INN", "T_\\d+_\\w+MAIL"
    );
    
    var tmpblob = "tmpblob";
    var blobFields = new Array("BLOB_XML", "REQ_XML", "RESP_XML", "SYS_INPUTFILE", "SYS_OUTFILE", tmpblob);
    var blobBlockSize = 1000;
        
    //Do not change below this line
    function TwoDigits(s) {
        if (s < 10) {
            return "0" + s;
        } else {
            return s;
        }
    }

    function stringRepeat(s, c) {
        return new Array(c+1).join(s);
    }

    function CheckIgnoredField(FieldName) {
        var strInput     = new String(FieldName);
        var strFieldName = new String();
        for (var i = 0; i < IgnoreFields.length; i++) {
            strFieldName = IgnoreFields[i];
            strFieldName = strFieldName.toUpperCase();
            strFieldName = strFieldName.substr(0, 30);
            if (strFieldName == strInput) {
                return true;
            }
        }
        return false;
    }

    function CheckDePersonalise(FieldName) {
        if(!NeedDepersonalise) {
            return false;
        }
        var re;
        for (var i = 0; i < DePersonalise.length; i++) {
            re = new RegExp(DePersonalise[i], "i");
            if (re.test(FieldName)) {
                return true; 
            }
        }
        return false;
    }

    function DoDepersonalise(Value, FieldName) {
        var v = new String(Value);
        var s;
        if (isNaN(Value)) {
            re = new RegExp("\\w+EMAIL", "i");
            re2 = new RegExp("\\w+PHONE", "i");
            if (re.test(FieldName)) {
                s = "t@t.com";
            } else if (re2.test(FieldName)) {
                // ����������� ��������� ����� �������� ��� ���������� ��������� ����� ��������
                s = "";
                for (var i=0; i<Value.length; i++) {
                    if (/^\d+$/.test(Value.charAt(i))) {
                        s = s + Math.ceil(Math.random()*9); // 1..9
                    } else {
                        s = s + Value.charAt(i);
                    }
                }
            } else {
                s = "�����������������������������������������������������������������������������������������������������������"; 
            }
        } else {
            s = "770708389324000000012345678901234567890";
        } //������� ���� ��� ����� ������ ��� 10, ��� � �� 12-�� �������� ��� ��������� ��������
        return "\'" + s.substr(0, Value.length) + "\'";
    }
      
    /******************************************************************
    * brief     
    *           function replaces needed chars by some symbols:
    *           "carriage return" with string "' || CHR(13) || '",
    *           "new line" replaces with string "' || CHR(10) || '",
    *           "carriage return" + "new line" replaces with string "' || CHR(13) || CHR(10) || '"
    *           "single quote" is doubled
    *           "double quote" is doubled
    *           returns string with escaped symbols
    * params
    *           Value [input] - the input string for symbol escaping
    *******************************************************************/
    function convertSymbols(strInputValue) {
        var strResult = new String(strInputValue);
        strResult = strResult.replace( /\'/g, "\'\'" );
        strResult = strResult.replace( /\r\n/g, "' || CHR(13) || CHR(10) || '" );
        strResult = strResult.replace( /\r/g,   "' || CHR(13) || '" );
        strResult = strResult.replace( /\n/g,   "' || CHR(10) || '" );
        return strResult;
    }

    function WriteTable(TableName, conn, out, sqlWhere, getField) {
        var retVal = [];
        WScript.Echo("Querying table " + TableName);
        //Open recordset
        rs = WScript.CreateObject("ADODB.Recordset");
        if (!sqlWhere) {
            sqlWhere = "SYS_RECORDKEY=" + SysRecordkey;
        }
        rs.Open("SELECT * FROM " + TableName + " WHERE " + sqlWhere, conn, 0 /* adOpenForwardOnly */, 1 /* adLockReadOnly */, 1 /* adCmdText */);

        if (rs.EOF) { 
            WScript.Echo("  " + TableName + " - record not found");
            return retVal; 
        }
        var outstr;
        while (!rs.EOF)    {
            outstr = "INSERT INTO " + TableName + "(" + "\r\n";

            //Preparing list of fields;
            var HookHistokeyNum;
            var AllFields = [];
            for (i = 0; i < rs.Fields.Count; i++) {
                if (rs.Fields(i).Type == 204 || CheckIgnoredField(rs.Fields(i).Name) || (rs.Fields(i).Type == 205 && !NeedBlob)) {
                    continue;
                }
                tf = "\"" + rs.Fields(i).Name + "\"";
                if ((i >= 20) && (i%20 == 0)) {
                    tf += "\r\n";
                }
                if (rs.Fields(i).Name.toUpperCase() == "HOOK_HISTOKEY") {
                    HookHistokeyNum=i;
                }
                AllFields.push(tf);
                //debug - write all fields types
                //WScript.Echo(rs.Fields(i).Name + "=" + rs.Fields(i).Type);
            }

            outstr += AllFields.join(",") + ")" + "\r\n";
            outstr += "values (" + "\r\n";

            //Writing data line by line
            var s = new String();
            var fldDate;
            var i;
            var Fld;
            var FldValue;
            var allValues = [];

            for (i = 0; i < rs.Fields.Count; i++) {
                if (rs.Fields(i).Type == 204 || CheckIgnoredField(rs.Fields(i).Name) || (rs.Fields(i).Type == 205 && !NeedBlob)) {
                    continue;
                }
                
                Fld = rs.Fields(i);
                FldValue = Fld.Value;

                if (Fld.Name == "SYS_RECORDKEY" || Fld.Name == "SYS_TABLEKEY" || Fld.Name == "SYS_HISTOKEY" || Fld.Name == "T_4450_APP_NO") {
                    FldValue = parseInt(Fld.Value) + parseInt(KeyIncrement);
                }

                if (i == HookHistokeyNum) {
                    s = (Fld.Value + 1) * -1;
                } else if (Fld.Value != null || Fld.Type == 205) {
                    if (getField != "" && getField == Fld.Name) {
                        retVal.push(Fld.Value);
                    }
                    switch (Fld.Type) {
                        case 204: continue; //skipping RAW fields
                        case 205: // blob
							if (NeedBlob) {
								s = blobField2String(TableName, Fld.Name, FldValue, out, rs);
                            } else {
                                continue; // skip blob fields
                            }
                            break;
                        case 129: // adChar
                        case 130: // adWChar
                        case 200: // adVarChar
                        case 202: // adVarWChar
                            if (CheckDePersonalise(Fld.Name)) {
                                s = DoDepersonalise(FldValue, Fld.Name);
                            } else {
                                s = "\'" + convertSymbols(FldValue) + "\'";
                            }
                        break;
                        //TODO: add timezone to date and time values
                        // now we assume Moscow TZ - GMT+3 (UTC+3, UTC+4)
                        case 133: // adDBDate
                            fldDate = new Date(FldValue);
                            s = "to_date(\'" +
                                TwoDigits(fldDate.getDate()) + "." + 
                                TwoDigits(fldDate.getMonth() + 1) + "." + 
                                TwoDigits(fldDate.getFullYear()) +
                                "\', \'DD.MM.YYYY\')";
                            break;
                        case 134: // adDBTime
                            fldDate = new Date(FldValue);
                            s = "to_timestamp(\'" +
                                TwoDigits(fldDate.getHours()) + ":" +
                                TwoDigits(fldDate.getMinutes()) + ":" +
                                TwoDigits(fldDate.getSeconds()) + "," +
                                fldDate.getMilliseconds() +
                                "\', \'HH24:MI:SS,FF\')";
                            break;
                        case 135: // adDBTimeStamp
                            fldDate = new Date(FldValue);
                            s = "to_timestamp(\'" +
                                TwoDigits(fldDate.getDate()) + "." + 
                                TwoDigits(fldDate.getMonth() + 1)+ "." + 
                                fldDate.getFullYear() + " " +
                                TwoDigits(fldDate.getHours()) + ":" +
                                TwoDigits(fldDate.getMinutes()) + ":" +
                                TwoDigits(fldDate.getSeconds()) + "," +
                                fldDate.getMilliseconds() +
                                "\', \'DD.MM.YYYY HH24:MI:SS,FF\')";
                            break;
                        default:
                            if (CheckDePersonalise(Fld.Name)) {
                                s = DoDepersonaliseProc(FldValue, Fld.Name);
                            } else {
                                s = convertSymbols(FldValue);
                            }
                            break;
                    }
                } else {
                    s = "null";        
                }
                if (s == null) {
                    s = "null"
                }
                if ((i >= 20) && (i%20 == 0)) {
                    s += "\r\n";
                }
                allValues.push(s);
            }
            outstr += allValues.join(",") + ");";
            out.WriteLine(outstr);
            rs.MoveNext;
        }
        rs.Close();
        return retVal;
    }
      
    function fullWriteTable(tableFullName,conn,out) {
        out.WriteLine("--");
        out.WriteLine("-- Table " + tableFullName);
        out.WriteLine("--");
        WriteTable(tableFullName, conn, out);
        out.WriteLine("--");
        out.WriteLine("-- End of table " + tableFullName);
        out.WriteLine("--");
    }

    function blobField2String(TableName, fldname, val, out, rs) {
        if (val != null) {
			// create temporary file
            fso    = WScript.CreateObject("Scripting.FileSystemObject");
			tf    = fso.GetSpecialFolder(2); // TemporaryFolder
			tn    = fso.GetTempName();
			tmpfile = tf + tn;

            // save blob stream
            tmp = WScript.CreateObject("ADODB.Stream");
            tmp.Charset = "UTF-8";
            tmp.Open();
            tmp.Type = 1;
            tmp.Write(val);
            tmp.SaveToFile(tmpfile, 2);
            // read blob from stream

            tmp.Type = 2;
            tmp.LoadFromFile(tmpfile);
            result = tmp.ReadText();
            tmp.Close();
			
			if (fldname == "SYS_INPUTFILE" || fldname == "SYS_OUTFILE") {
				result = BinaryFileReader.ReadAllBytesFromFile(tmpfile);
				out.WriteLine("DELETE FROM TMP_BLOBPARTS;");
				out.WriteLine("");
				var i = 1;
				while (result) {
					tmpresult = result.substr(0, blobBlockSize);
					out.WriteLine("INSERT INTO TMP_BLOBPARTS");
					out.WriteLine("   (partnum, blobpart)");
					out.WriteLine("values");
					out.WriteLine("   ("+ i + ", '" + tmpresult + "');");
					out.WriteLine("");
					result = result.substr(blobBlockSize);
					i++;
				}
				out.WriteLine("SYS.dbms_lob.createtemporary(blob_" + fldname + ", true);");					
				out.WriteLine("");  
				out.WriteLine("FOR X IN (SELECT * FROM TMP_BLOBPARTS T ORDER BY T.PARTNUM) LOOP ");  
				out.WriteLine(" SYS.DBMS_LOB.APPEND(blob_" + fldname + ", X.BLOBPART); ");  
				out.WriteLine("END LOOP; ");  
				out.WriteLine("");  					
			} else {
				out.WriteLine("SYS.dbms_lob.createtemporary(blob_" + fldname + ", true);");
				while (result) {
					tmpresult   = result.substr(0, blobBlockSize);
					tmpresult = tmpresult.replace( /\'/g, "\'\'" )
					tmpresult   = "blob_" + tmpblob + " := SYS.utl_raw.cast_to_raw('" + tmpresult + "');";
					out.WriteLine(tmpresult);
					out.WriteLine("SYS.dbms_lob.APPEND(blob_" + fldname + ", blob_" + tmpblob + ");");
					result = result.substr(blobBlockSize);
				}
			}
			fso.DeleteFile(tmpfile);						
			return "blob_"+fldname;
        }
        return "null";
    }

    //Begin
    //Check if there are arguments present
    if (WScript.Arguments.length >= 4) {
        ConnectionString = "Data Source=" + WScript.Arguments(0) + ";User Id=" + WScript.Arguments(1) + ";Password=" + WScript.Arguments(2);
        AppNo = WScript.Arguments(3);
        try {
            Prefix = WScript.Arguments(4);
        } catch (e) {
            Prefix = "";
        }
        try {
            if (WScript.Arguments(5) == "no") {
                NeedDepersonalise = false; 
                WScript.Echo("NO DEPERSONALISATION! Result can't be sent outside of bank!");
            }
        } catch (e) {
            NeedDepersonalise = true;
        }
    }
    else {
        // Arguments                       0            1        2          3        4                   5                            6					7
        WScript.Echo("Arguments required: <DataSource> <UserId> <Password> <AppNo> [<filename_Prefix>] [<depersonalisation yes/no>] [key increment] [need blob]");
        WScript.Quit(1);
    }

    KeyIncrement = 0;
    
    try {
        if (WScript.Arguments(7) != "") {
            KeyIncrement = WScript.Arguments(7);
            WScript.Echo("KeyIncrement="+KeyIncrement);
            WScript.Echo("SYS_RECORDKEY, SYS_TABLEKEY, SYS_HISTOKEY and T_4450_APP_NO will be incremented by " + KeyIncrement);
            //WScript.Echo("Older record won't be deleted");
            //DoDelete = false;
        }
    } catch (e) {
        KeyIncrement = 0;
    }
    try {
        if (WScript.Arguments(6) == "yes") {
            NeedBlob = true;
            WScript.Echo("BlobFile will be created");
        }
    } catch (e) {
        NeedBlob = false;
    }

    //Open ADO connection
    conn = WScript.CreateObject("ADODB.Connection");
    conn.ConnectionString = ConnectionString;
    conn.Open;
    WScript.Echo("Connected");

    var rk = WScript.CreateObject("ADODB.Recordset");
    WScript.Echo("Extracting application " + WScript.Arguments(0));

    rk.Open("select SYS_RECORDKEY from " + Schema + ".APPLICANTS WHERE T_4450_APP_NO=" + AppNo,
        conn, 0 /* adOpenForwardOnly */, 1 /* adLockReadOnly */, 1 /* adCmdText */
    );
    if (rk.EOF) {
        WScript.Echo("Application not found");
        WScript.Quit(2);
    }
    SysRecordkey = rk.Fields("SYS_RECORDKEY").Value;
    WScript.Echo("SYS_RECORDKEY=" + SysRecordkey);
    OutFileName = AppNo + ".sql";
    if (WScript.Arguments.length > 4) {
        OutFileName = Prefix + OutFileName;
    }
    rk.Close();

    fso = WScript.CreateObject("Scripting.FileSystemObject");
    //Creating output SQL
    /*
    * Tristate
    * -1 - Unicode
    * 0  - ASCII
    * -2 - file system default
    */
    var out = fso.OpenTextFile(OutFileName, 2 /* ForWriting */, true, -1/* Tristate */);

    out.WriteLine("/* " + AppNo + " */");
    out.WriteLine("SET DEFINE OFF;");
    out.WriteLine("");
    out.WriteLine("BEGIN");
	out.WriteLine("  EXECUTE IMMEDIATE 'create table TMP_BLOBPARTS (partnum  INTEGER not null, blobpart BLOB)';");
    out.WriteLine("END;");
    out.WriteLine("/");
    out.WriteLine("");

    out.WriteLine("DECLARE");
    for (var b = 0; b < blobFields.length; b++) {
        out.WriteLine("blob_" + blobFields[b] + " BLOB;");
    }
    out.WriteLine("BEGIN");
  
    if (DoDelete) {
        out.WriteLine("DELETE FROM " + Schema + ".HISTORIC WHERE SYS_RECORDKEY=" + (parseInt(SysRecordkey) + parseInt(KeyIncrement)) + ";");
        for (var i = 0; i < Tables.length; i++) {
            if (Tables[i] != "APPLICANTS") {
                out.WriteLine("DELETE FROM " + Schema + "." + Tables[i] + " WHERE SYS_RECORDKEY=" + (parseInt(SysRecordkey) + parseInt(KeyIncrement)) + ";");
            }
        }
        out.WriteLine("DELETE FROM " + Schema + ".APPLICANTS WHERE SYS_RECORDKEY=" + (parseInt(SysRecordkey) + parseInt(KeyIncrement)) + ";");
    }
      
    for (var i = 0; i < Tables.length; i++) {
        fullWriteTable(Schema + "." + Tables[i],conn,out);
    }
    fullWriteTable(Schema + ".HISTORIC", conn, out);

    // CBORD / CBXRD
    out.WriteLine("DELETE FROM SME.CBORD WHERE SYS_APPKEY=" + SysRecordkey + ";");
    WriteTable("SME.CBORD", conn, out, "SYS_APPKEY=" + SysRecordkey);
    out.WriteLine("DELETE FROM SME.CBXRD WHERE SYS_APPKEY=" + SysRecordkey + ";");
    WriteTable("SME.CBXRD", conn, out, "SYS_APPKEY=" + SysRecordkey);
    // CB
    out.WriteLine("DELETE FROM UG_SBB.CB_REPORT WHERE APP_NO=" + AppNo + ";");
    var REP_IDs = WriteTable("UG_SBB.CB_REPORT", conn, out, "APP_NO=" + AppNo, "REP_ID");
    if (REP_IDs.length > 0) {
        out.WriteLine("DELETE FROM UG_SBB.CB_LIABILITY WHERE REP_ID IN (" + REP_IDs.join(",") + ");");
        var LIAB_IDs = WriteTable("UG_SBB.CB_LIABILITY", conn, out, "REP_ID IN (" + REP_IDs.join(",") + ")");
        if (LIAB_IDs.length > 0) {
            out.WriteLine("DELETE FROM UG_SBB.CB_ARREARS WHERE LIAB_ID IN (" + LIAB_IDs.join(",") + ");");
            WriteTable("UG_SBB.CB_ARREARS", conn, out, "LIAB_ID IN (" + LIAB_IDs.join(",") + ")");
        }
    }
    // GRB
    var grbTables = ["GRB_CUSTOMERS", "GRB_LIABILITIES", "GRB_RELATIONS"];
    out.WriteLine("DELETE FROM UG_SBB.GRB_APPLICATION WHERE APP_TSM_NO=" + AppNo + ";");
    var UQIDs = WriteTable("UG_SBB.GRB_APPLICATION", conn, out, "APP_TSM_NO=" + AppNo, "UQID");
    if (UQIDs.length > 0) {
        for (var i = 0; i < grbTables.length; i++) {
            out.WriteLine("DELETE FROM UG_SBB." + grbTables[i] + " WHERE UQID IN (" + UQIDs.join(",") + ");");
            WriteTable("UG_SBB." + grbTables[i], conn, out, "UQID IN (" + UQIDs.join(",") + ")");
        }
    }
    // BLOB
    var ID    = stringRepeat("0", 20 - AppNo.length) + AppNo;
    out.WriteLine("DELETE FROM UG_SBB.UG_S_BLOB WHERE ID LIKE '" + ID + "%';");
    WriteTable("UG_SBB.UG_S_BLOB", conn, out, "ID LIKE '" + ID + "%'");
    
    // IN_OUT_LOG
    out.WriteLine("DELETE FROM UG_SBB.IN_OUT_LOG WHERE APP_NO='" + AppNo + "';");
    WriteTable("UG_SBB.IN_OUT_LOG", conn, out, "APP_NO='" + AppNo + "'");
    
    // BRS
    /*for (var b = 0; b < BRSTables.length; b++) {
        out.WriteLine("DELETE FROM UG_SBB."+BRSTables[b]+" WHERE MESS_UID LIKE '" + ID + "%';");
        WriteTable("UG_SBB."+BRSTables[b]+"", conn, out, "MESS_UID LIKE '" + ID + "%'");
    }*/
    
    // Pledge
    for (var b = 0; b < PledgeTables.length; b++) {
        out.WriteLine("DELETE FROM SME_HOOKS."+PledgeTables[b]+" WHERE SYS_RECORDKEY=" + SysRecordkey);
        WriteTable("SME_HOOKS."+PledgeTables[b]+"", conn, out, "SYS_RECORDKEY=" + SysRecordkey);
    }
    
    // MDM
    var mdmTables = ["UG_MDM_DATA", "UG_MDM_DATA_L"];
    out.WriteLine("DELETE FROM UG_SBB.UG_MDM WHERE APP_NO=" + AppNo + ";");
    var mdmIDs = WriteTable("UG_SBB.UG_MDM", conn, out, "APP_NO=" + AppNo, "ID");
    if (mdmIDs.length > 0) {
        for (var i = 0; i < mdmTables.length; i++) {
            out.WriteLine("DELETE FROM UG_SBB." + mdmTables[i] + " WHERE ID IN (" + mdmIDs.join(",") + ");");
            WriteTable("UG_SBB." + mdmTables[i], conn, out, "ID IN (" + mdmIDs.join(",") + ")");
        }
    }

    out.WriteLine("END;");
    out.WriteLine("/");	

    out.WriteLine("BEGIN");
    out.WriteLine("  EXECUTE IMMEDIATE 'DROP TABLE TMP_BLOBPARTS';");
    out.WriteLine("END;");
    out.WriteLine("/");	
    out.Close();

    WScript.Echo("Output script " + OutFileName + " succesfully written");
    //End
