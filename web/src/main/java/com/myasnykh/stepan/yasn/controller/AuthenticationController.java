package com.myasnykh.stepan.yasn.controller;


import com.getjavajob.yasn.myasnykh.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
public class AuthenticationController {
@Autowired
ProfileService profileService;
    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public ModelAndView getLogInPage(HttpSession session) {
        return new ModelAndView("login");
    }
}
