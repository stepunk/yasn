<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en" xmlns:width="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <title>Bootstrap 101 Template</title>
    <link href="<c:url value="/static/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/static/css/yasn.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
</head>
<body>
<div class="container" id="profile_page">
    <jsp:include page="header.jsp"></jsp:include>
    <div class="container-fluid">
        <div class="row">
            <jsp:include page="navigation.jsp"></jsp:include>
            <div class="col-md-10 main" style="border-left:1px solid #ccc">
                <c:choose>
                    <c:when test="${not empty error}">
                        <div class="container-fluid text-center">
                            <h2 class="error">
                                    ${error.message}
                            </h2>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="container-fluid">

                            <div class="well">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#ProfileInfoBlock">Profile</a></li>
                                    <li><a data-toggle="tab" href="#PhotoBlock">Photo</a></li>
                                    <li><a data-toggle="tab" href="#SecurityBlock">Security</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div id="ProfileInfoBlock" class="tab-pane fade in active">


                                        <form class="form-horizontal" action="settings" method="post" role="form">

                                            <fieldset>

                                                <div id="Info">
                                                    <legend class="text-center">Profile Info</legend>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <label for="firstName">First Name</label>
                                                        <input class="form-control" type="text" id="firstName"
                                                               name="firstName"
                                                               value="${profile.firstName}" required>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label for="lastName">Last Name</label>
                                                        <input class="form-control" type="text" id="lastName"
                                                               name="lastName"
                                                               value="${profile.lastName}" required>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <label for="email">Birthday</label>
                                                        <input class="form-control" type="date" id="birthDay"
                                                               name="birthDay"
                                                               value="<fmt:formatDate type="date"
                                                           pattern="yyyy-MM-dd"
                                                           value="${profile.birthDay}"/>" required>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <label for="email">Email</label>
                                                        <input class="form-control" type="email" id="email" name="email"
                                                               value="${profile.email}"
                                                               readonly>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <label for="countries">Country</label>
                                                        <select class="form-control" id="countries" name="country">
                                                            <c:choose>
                                                                <c:when test="${not empty profile.country}">
                                                                    <option value="${profile.country.countryId}"
                                                                            selected>${profile.country}</option>
                                                                    <option value="0">---</option>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <option value="0" selected>---</option>
                                                                </c:otherwise>
                                                            </c:choose>

                                                            <c:forEach items="${requestScope.countries}" var="country">
                                                                <option value="${country.countryId}">${country.country}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <label for="cities">City</label>
                                                        <select class="form-control" id="cities" name="city"
                                                                value="${profile.city}">
                                                            <c:choose>
                                                                <c:when test="${not empty profile.city}">
                                                                    <option value="${profile.city.cityId}"
                                                                            selected>${profile.city}</option>
                                                                    <option value="0">---</option>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <option value="0" selected>---</option>
                                                                </c:otherwise>
                                                            </c:choose>

                                                            <c:forEach items="${requestScope.cities}" var="city">
                                                                <option value="${city.cityId}">${city.city}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div id="Change Password">
                                                    <legend class="text-center">Change password</legend>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <label for="email">Current Password</label>
                                                        <input class="form-control" type="password" id="currentPassword"
                                                               name="currentPassword"
                                                               placeholder="Input your current password">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <label for="email">New password</label>
                                                        <input class="form-control" type="password" id="password"
                                                               name="password" placeholder="Input new password">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <label for="email">Repeat new password</label>
                                                        <input class="form-control" type="password" id="confirmPassword"
                                                               name="confirmPassword" placeholder="Repeat new password">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-primary ">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div id="SecurityBlock" class="tab-pane fade">

                                        <c:choose>
                                            <c:when test="${not empty error}">
                                                <h2 class="personInfo">${error.text}
                                                </h2>
                                            </c:when>
                                            <c:otherwise>
                                                <form class="form-horizontal" id="SecurityForm" action="security"
                                                      method="post"
                                                      role="form">

                                                    <fieldset>

                                                        <div id="Srcurity">
                                                            <legend class="text-center">Security Settings</legend>
                                                        </div>
                                                        <div id="updated">

                                                        </div>

                                                        <c:forEach items="${requestScope.settings}" var="settings">
                                                            <c:set var="setting" value="${settings.key}" scope="page"/>

                                                            <div class="form-group">
                                                                <div class="col-md-4">
                                                                    <label for="${settings.key}">${settings.key}</label>
                                                                    <input list="${settings.key}"
                                                                           class="form-control"
                                                                           type="text"
                                                                           name="${settings.key}"
                                                                           value="${requestScope.userSettings[setting]}"
                                                                           required>
                                                                    <datalist id="${settings.key}">
                                                                        <c:forEach items="${settings.value}"
                                                                                   var="option">
                                                                            <option value="${option}">${option}</option>
                                                                        </c:forEach>
                                                                    </datalist>
                                                                        <%--</div>--%>
                                                                </div>
                                                            </div>
                                                        </c:forEach>

                                                            <%--<c:forEach items="${requestScope.settings}"--%>
                                                            <%--var="settings">--%>
                                                            <%--<c:set var="setting" value="${settings.key}"--%>
                                                            <%--scope="page"/>--%>

                                                            <%--<div class="form-group">--%>
                                                            <%--<div class="col-md-4">--%>
                                                            <%--<label for="${settings.key}">${settings.key}</label>--%>
                                                            <%--<select class="form-control"--%>
                                                            <%--id="${settings.key}"--%>
                                                            <%--name="${settings.key}"--%>
                                                            <%--required>--%>
                                                            <%--<c:choose>--%>
                                                            <%--<c:when test="${not empty requestScope.userSettings[setting]}">--%>
                                                            <%--<option value="${requestScope.userSettings[setting]}"--%>
                                                            <%--selected>${requestScope.userSettings[setting]}</option>--%>
                                                            <%--</c:when>--%>
                                                            <%--</c:choose>--%>
                                                            <%--<c:forEach items="${settings.value}"--%>
                                                            <%--var="option">--%>
                                                            <%--<option value="${option}">${option}</option>--%>
                                                            <%--</c:forEach>--%>
                                                            <%--</select>--%>
                                                            <%--</div>--%>
                                                            <%--</div>--%>
                                                            <%--</c:forEach>--%>

                                                    </fieldset>
                                                </form>
                                                <div class="text-center">
                                                    <button id="saveSecurity" class="btn btn-primary ">Save</button>
                                                </div>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                    <div id="PhotoBlock" class="tab-pane fade">
                                        <div class="registration">
                                            <br>

                                            <div id="photo">
                                                    <%--<img class="img-responsive " src="getImage/${sessionScope.userId}"--%>
                                                    <%--style="width:200px">--%>
                                            </div>

                                            <br>
                                            <br>

                                            <form id="uploadImage" action="storeImage" method="post"
                                                  enctype="multipart/form-data">
                                                <input type="file" style="width:25%" id="Image" name="Image">
                                                <br>
                                            </form>

                                            <div class="text-center" id="savePhoto">
                                                <button class="btn btn-primary" id="saveImage">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>
<jsp:include page="footer.jsp"></jsp:include>
<script>
    $("#countries").change(function () {
        console.log("a button was clicked");
        var countryId = $(this).val();
        $.get("cities/" + countryId, function (data) {
            $("#cities option").remove();
            $("#cities").append("<option value='0'>---</option>");
            for (var i = 0; i < data.length; i++) {
                $("#cities").append("<option value='" + data[i].cityId + "'>" + data[i].city + "</option>");
            }
        })
    });


    $("#saveImage").click(function () {
        console.log("a button was clicked");
        document.getElementById('photo').innerHTML = '';
        var formData = new FormData();
        var file = document.getElementById("Image").files[0];
        formData.append("Image", file);


        $.ajax({
            url: 'storeImage',
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                if (data) {
                    $("#photo").append(
                            "<img class='img-responsive' src='getImage/${sessionScope.userId}'" +
                            "style = 'width:200px' >"
                    );
                } else {
                    $("#photo").append(
                            "<h4>photo was not stored</h4>"
                    );
                }
            }
        });
    });

    $("#saveSecurity").click(function () {
        console.log("a button was clicked");
        document.getElementById('updated').innerHTML = '';
        var formData = $("#SecurityForm").serialize();

        $.ajax({
            type: "Post",
            url: "security",
            data: formData,
            success: function (data) {
                if (data) {
                    $("#updated").append(
                            "<h4> Settings was updated</h4>"
                    );
                } else {
                    $("#updated").append(
                            "<h4> Settings was not updated</h4>"
                    );
                }

            }
        });

//        return false; // avoid to execute the actual submit of the form.
    });
</script>
</body>
</html>