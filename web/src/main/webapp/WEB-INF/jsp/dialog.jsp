<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>YASN</title>
    <link href="<c:url value="/static/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/static/css/yasn.css"/>" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
</head>
<body>
<div class="container" id="profile_page">
    <jsp:include page="header.jsp"></jsp:include>
    <div class="container-fluid">
        <div class="row">
            <jsp:include page="navigation.jsp"></jsp:include>
            <div class="col-md-10 main" id="content" style="border-left:1px solid #ccc">
                <div class='row text-center'>
                    <h4>You have ${fn:length(requestScope.messages)} messages</h4>
                </div>
                <%--<c:set var="userId" value="${requestScope.user}"></c:set>--%>
                <%--<c:set var="companionId" value="${requestScope.companion}"></c:set>--%>
                <c:set var="lastWriter" value=""></c:set>
                <c:forEach var="message" items="${requestScope.messages}">
                    <div class="row" style="margin-top: 10px">

                        <c:choose>
                            <c:when test="${lastWriter != message.authorId}">
                                <div class="col-md-2">
                                    <a href="user?id=${message.authorId.userId}"><img
                                            class="img-responsive center-block"
                                            src="getImage/${message.authorId.userId}"
                                            alt="photo authorId = ${message.authorId.userId}"
                                            style="height:50px"></a>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <a href="user?id=${message.authorId.userId}">${message.authorId.firstName}</a>
                                    </div>
                                    <div class="row">
                                        <p class="text-left">${message.text}</p>
                                    </div>
                                </div>

                                <c:set var="lastWriter" value="${message.authorId}"></c:set>
                            </c:when>
                            <c:otherwise>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <p class="text-left">${message.text}</p>
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>


                        <div class="col-md-2">
                            <div class="row">
                                <%--<p class="text-info">--%>
                                    <%--<fmt:formatDate type="date"--%>
                                                    <%--pattern="dd MMM"--%>
                                                    <%--value="${message.date}"/> at--%>
                                    <%--<fmt:formatDate type="time"--%>
                                                    <%--pattern="h : m a"--%>
                                                    <%--value="${message.date}"/>--%>
                                <%--</p>--%>
                            </div>
                        </div>
                    </div>
                </c:forEach>
                <div id="endDialog">

                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2" style="margin-top: 10px;">
                        <form action="dialog" id="postForm" method="post" role="form">
                            <div class="form-group" style="margin-bottom:3px ">
                                <%--<input type="hidden" id="author" name="author" value="${user.userId}">--%>
                                <input type="hidden" id="companion" name="companion" value="${companionId}">
                                    <textarea class="form-control" rows="2" id="message" name="text"
                                              placeholder="start typing"></textarea>
                            </div>
                        </form>
                        <button type="submit" class="btn btn-primary" id="postButton">Submit</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<jsp:include page="footer.jsp"></jsp:include>

<script>
    var lastWriterId=${lastWriter.userId};
    setInterval(function () {
        $.get("dialog?id=${companionId}", function (data) {
            if(data.length>0) {
                for (var i = 0; i < data.length; i++) {
                    postMessage(data[i])
                }
                lastWriterId = data[0].authorId.userId;
            }
        })
    }, 2000);


    $("#postButton").click(function () {
        var formData = $("#postForm").serialize();
        $.ajax({
            type: "POST",
            url: "dialog",
            data: formData,
            success: function (data) {
                postMessage(data);
            }
        });
    });

    var postMessage= function(data){
        if (data != null) {
//                    var messageTime = new Date(data.date);
            if (data.authorId.userId ==lastWriterId) {
                $("#endDialog").append(
                        "<div class='row' style='margin-top: 10px'>" +
                        "<div class='col-md-2'>" +
                        "</div>" +
                        "<div class='col-md-8'>" +
                        "<div class='row'>" +
                        "<p class='text-left'>" + data.text + "</p>" +
                        "</div>" +
                        "</div>" +
                        "<div class='col-md-2'>" +
                        "<div class='row'>" +
                        "<p class='text-info'>" +
                        <%--<fmt:formatDate type="date"--%>
                        <%--pattern="dd MMM"--%>
                        <%--value="${data.date}"/> "at" +--%>
                        <%--<fmt:formatDate type="time"--%>
                        <%--pattern="h : m a"--%>
                        <%--value="${data.date}"/>--%>
                        "</p>" +
                        "</div>" +
                        "</div>" +
                        "</div>"
                )
            }
            else {
                $("#endDialog").append(
                        "<div class='row' style='margin-top: 10px'>" +
                        "<div class='col-md-2'> " +
                        "<a href='user?id=" + data.authorId.userId + "'>" +
                        "<img class= 'img-responsive center-block' src = 'getImage/" + data.authorId.userId + "'" +
                        "alt = 'photo authorId = " + data.authorId.userId + "'" +
                        "style = 'height:50px'></a> " +
                        "</div >" +
                        "<div class = 'col-md-8' >" +
                        "<div class = 'row' >" +
                        "<a href = 'user?id=" + data.authorId.userId + "'>" + data.authorId.firstName + "</a >" +
                        "</div>" +
                        "<div class= 'row' >" +
                        "<p class= 'text-left'>" + data.text + "</p>" +
                        "</div>" +
                        "</div >" +
                        <%--<c:set var="lastWriter" value="${data.authorId}"/>--%>
                        "<div class='col-md-2'>" +
                        "<div class='row'>" +
                        "<p class='text-info'>" +
//                               messageTime.getHours()+
                        <%--<fmt:formatDate type="date"--%>
                        <%--pattern="dd MMM"--%>
                        <%--value="${data.date}"/> "at" +--%>
                        <%--<fmt:formatDate type="time"--%>
                        <%--pattern="h : m a"--%>
                        <%--value="${data.date}"/>--%>
                        "</p>" +
                        "</div>" +
                        "</div>" +
                        "</div>"
                );
                lastWriterId = data.authorId.userId;
            }
        }
    }
</script>
</body>
</html>