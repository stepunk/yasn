<div class="navbar navbar-static-top" role="navigation" id="menu_top">
  <div class="container-fluid navbar-yasn" style="border-radius: 10px 10px 10px 10px">
    <div>
      <a class="navbar-brand" href="login">YASN</a>
    </div>

    <div class="col-md-4 col-md-offset-1">
      <form class="navbar-form navbar-left" action="search">
        <input type="text" class="form-control" placeholder="Search..." id="name" name="name">
        <button type="submit" class="btn btn-default">Search</button>
      </form>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="logout">log out</a></li>
      </ul>
    </div>
  </div>
</div>