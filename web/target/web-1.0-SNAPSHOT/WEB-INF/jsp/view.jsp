<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>YASN</title>
    <link href="<c:url value="/static/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/static/css/yasn.css"/>" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
</head>
<body>
<div class="container" id="profile_page">
    <jsp:include page="header.jsp"></jsp:include>
    <div class="container-fluid">
        <div class="row">
            <jsp:include page="navigation.jsp"></jsp:include>
            <div class="col-md-10 main" id="content" style="border-left:1px solid #ccc">
                <c:choose>
                    <c:when test="${not empty error}">
                        <div class="container-fluid text-center">
                            <h2 class="error">
                                    ${error.message}
                            </h2>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="container-fluid">
                            <c:choose>
                                <c:when test="${(not empty securityRestriction) and not securityRestriction['can view profile']}">
                                    <h2 class="error">You can't view profile</h2>
                                </c:when>
                                <c:otherwise>
                                    <div class="row">
                                        <div class="col-md-4 main" id="page_avatar">
                                            <div class="row  center-block">
                                                <img class="img-responsive " src="getImage/${user.userId}"
                                                     alt="photo  ${user.firstName} ${user.lastName}"
                                                     style="width:200px">
                                            </div>
                                            <c:choose>
                                                <c:when test="${not(sessionScope.userId==user.userId)}">
                                                    <div class="row  center-block" id="profile_main_actions"
                                                         style="margin-top: 1px">
                                                        <div class="col-md-9">
                                                            <button type="button"
                                                                    class="btn btn-primary btn-mn btn-block">
                                                                Send
                                                                message
                                                            </button>

                                                            <button type="button" id="addFriend"
                                                                    class="btn btn-default btn-mn btn-block">
                                                                Add
                                                                to
                                                                friends
                                                            </button>
                                                        </div>
                                                    </div>
                                                </c:when>
                                            </c:choose>
                                        </div>
                                        <div class="col-md-8 main" id="profile_info">
                                            <h3 class="personInfo">${user.firstName} ${user.lastName}
                                            </h3>
                                            <c:choose>
                                            <c:when test="${not empty user.city}">
                                            <h4 class="personInfo">${user.city.city},
                                                </c:when>
                                                <c:otherwise>
                                                <h4 class="personInfo">
                                                    </c:otherwise>
                                                    </c:choose>
                                                        ${user.country.country}
                                                </h4>
                                            </h4>
                                            <h4 class="personInfo"><fmt:formatDate type="date"
                                                                                   pattern="dd MMMMMM yyyy"
                                                                                   value="${user.birthDay}"/>
                                            </h4>
                                        </div>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <jsp:include page="wall.jsp"></jsp:include>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>


<jsp:include page="footer.jsp"></jsp:include>
<script>
    $("#addFriend").click(function () {
        console.log("a button was clicked");

        $.get("addFriend/" + ${user.userId}, function (data) {
            if (data) {
                console.log("data=" + data);
            } else {
                console.log("data=" + data)
            }
        })
    });
</script>
</body>
</html>