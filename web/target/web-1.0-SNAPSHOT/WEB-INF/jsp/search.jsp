<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>YASN</title>
    <link href="<c:url value="/static/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/static/css/yasn.css"/>" rel="stylesheet">
    <link href="<c:url value="/static/css/yasn-search-filter.css"/>" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
    <script src="<c:url value="/static/js/bootstrap.min.js"/>"></script>
</head>
<body>
<div class="container" id="profile_page">
    <jsp:include page="header.jsp"></jsp:include>
    <div class="container-fluid">
        <div class="row">
            <jsp:include page="navigation.jsp"></jsp:include>
            <div class="col-md-10 main" style="border-left:1px solid #ccc">
                <form id="searchForm" name="searchForm" method="get" class="form-horizontal" role="form">
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                            <div id="imaginary_container">
                                <div class="input-group stylish-input-group">
                                    <input type="text" id="name" name="name" class="form-control"
                                           placeholder="Input name"
                                           value="${filter.fullName}">
                                     <span class="input-group-addon">
                                        <button type="submit" id="searchButton">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                     </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="filter-panel" class="collapse filter-panel well">
                        <%--<div class="panel panel-default">--%>
                        <div class="panel-body">
                            <div class="col-md-4 col-md-offset-4">

                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="ageFrom">Age from:</label>
                                        <input class="form-control" type="text" id="ageFrom" name="ageFrom"
                                               value="${filter.ageFrom}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="ageTo">Age to:</label>
                                        <input class="form-control" type="text" id="ageTo" name="ageTo"
                                               value="${filter.ageTo}">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="gender">Gender</label>

                                        <div class="btn-group" data-toggle="buttons" id="gender">
                                            <label class="btn btn-default">
                                                <input type="radio" name="gender" value="male"/> Male
                                            </label>
                                            <label class="btn btn-default">
                                                <input type="radio" name="gender" value="female"/> Female
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="countries">Country</label>
                                        <select class="form-control" id="countries" name="country">
                                            <option selected value="">None selected</option>
                                            <c:forEach items="${requestScope.countries}" var="country">
                                                <option value="${country.countryId}">${country.country}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="cities">City</label>
                                        <select class="form-control" id="cities" name="city"
                                                value="${profile.city}">
                                            <option value="${filter.city}" selected>${filter.city}</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <%--</div>--%>
                    </div>
                    <button type="button" class="btn btn-primary center-block" data-toggle="collapse"
                            data-target="#filter-panel">
                        <span class="glyphicon glyphicon-filter"></span> Filter
                    </button>
                </form>
                <hr>
                <div id="show">
                    <c:choose>
                        <c:when test="${not empty requestScope.profiles}">
                            <c:forEach var="profile" items="${requestScope.profiles}">
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-3" id="photo"><a href="user?id=userId"><img
                                            class="img-responsive"
                                            src="getImage/${profile.userId}" alt="photo"
                                            style="width:200px;height:200px"></a>
                                    </div>
                                    <div class="col-md-9" id="info"><a href="user?id=${profile.userId}"><h3
                                            class="personInfo">${profile.firstName} ${profile.lastName}</h3>
                                    </a>
                                        <c:choose>
                                        <c:when test="${not empty profile.city}">
                                        <h4 class="personInfo">${profile.city.city},
                                            </c:when>
                                            <c:otherwise>
                                            <h4 class="personInfo">
                                                </c:otherwise>
                                                </c:choose>
                                                    ${profile.country.country}
                                            </h4>
                                        </h4>
                                    </div>
                                </div>
                            </c:forEach>
                        </c:when>
                    </c:choose>

                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="footer.jsp"></jsp:include>


<script>
    $("#countries").change(function () {
        console.log("a button was clicked");
        var countryId = $(this).val();
        if (countryId == "") {
            $("#cities option").remove();
            $("#cities").append("<option value=''>None selected</option>");
        } else {
            $.get("cities/" + countryId, function (data) {
                $("#cities option").remove();
                $("#cities").append("<option value=''>None selected</option>");
                for (var i = 0; i < data.length; i++) {
                    $("#cities").append("<option value='" + data[i].city + "'>" + data[i].city + "</option>");
                }
            })
        }
    });

    $("#searchButton").click(function () {
        document.getElementById('show').innerHTML = '';
        var formData = $("#searchForm").serialize();

        $.ajax({
            type: "GET",
            url: "searchByCriteria",
            data: formData,
            success: function (data) {
                if (data.length == 0) {
                    $("#show").append(
                            "<div class='row text-center'>" +
                            "<h4>Nothing found</h4>" +
                            "</div>"
                    )
                    ;
                } else {
                    $("#show").append(
                            "<div class='row text-center'>" +
                            "<h4>Found " + data.length + " users</h4>" +
                            "</div>"
                    )
                    for (var i = 0; i < data.length; i++) {
                        var city;
                        if (data[i].city != null) {
                            city = data[i].city.city + ", ";
                        } else {
                            city = "";
                        }
                        var country;
                        if (data[i].country != null) {
                            country = data[i].country.country;
                        } else {
                            country = "";
                        }
                        $("#show").append(
                                "<div class='row' style='margin-top: 10px'>" +
                                "<div class='col-md-3' id='photo'>" +
                                "<a href='user?id=" + data[i].userId + "'><img class='img-responsive' src=getImage/" + data[i].userId + " alt='photo'style='width:200px;height:200px'></a>" +
                                "</div>" +
                                "<div class='col-md-9' id='info'>" +
                                "<a href='user?id=" + data[i].userId + "'><h3 class='personInfo'>" + data[i].firstName + " " + data[i].lastName + "</h3></a>" +
                                "<h4 class='personInfo'>" + city + country + "</h4>" +
                                "</div>" +
                                "</div>"
                        )
                        ;
                    }
                }
            }
        });

        return false; // avoid to execute the actual submit of the form.
    });
</script>
</body>
</html>
