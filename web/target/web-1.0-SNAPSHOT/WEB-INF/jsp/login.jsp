<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>YASN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<c:url value="/static/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/static/css/yasn-login.css"/>" rel="stylesheet">
    <style type="text/css">
    </style>
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="<c:url value="/static/js/bootstrap.min.js"/>"></script>
</head>
<body>
<div class="container">
    <div class="card card-container">
        <img id="profile-img" class="profile-img-card" src="<c:url value="/static/img/signin.png"/>"/>

        <p id="profile-name" class="profile-name-card"></p>

        <form class="form-signin" action="<c:url value='/j_spring_security_check' />" method="post">
        <%--<form class="form-signin" action="login" method="post">--%>
            <span id="reauth-email" class="reauth-email"></span>
            <input type="email" id="email" name="email" class="form-control" placeholder="Email address" required
                   autofocus>
            <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <div id="remember" class="checkbox">
                <label>
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
        </form>
        <!-- /form -->
        <div class="panel-footer ">
            Don't have an account! <a href="register" onClick=""> Sign Up Here </a>
        </div>
    </div>
    <!-- /card-container -->
</div>
<!-- /container -->
</body>
</html>