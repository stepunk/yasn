<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>LogIn</title>
    <link href="<c:url value="/static/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/static/css/yasn-login.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
</head>
<body>
<div class="container">
    <div class="container-fluid" style="margin-top: 20px">
        <div class="col-md-10 main col-md-offset-1">
            <c:choose>
                <c:when test="${not empty error}">
                    <h2 style="color: red">Incorrect form</h2>
                    <h4>${error.message}</h4>
                </c:when>
            </c:choose>
            <div class="well">
                <form class="form-horizontal" action="register" method="post" role="form">
                    <div id="Info">
                        <legend class="text-center">Registration</legend>
                    </div>


                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="firstName">First Name</label>
                            <input class="form-control" type="text" id="firstName"
                                   name="firstName"
                                   value="${profile.firstName}" required>
                        </div>
                        <div class="col-md-4">
                            <label for="lastName">Last Name</label>
                            <input class="form-control" type="text" id="lastName"
                                   name="lastName"
                                   value="${profile.lastName}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="email">Email</label>
                            <input class="form-control" type="email" id="email" name="email"
                                   value="${profile.email}"
                                   required>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="password">Password</label>
                            <input class="form-control" type="password" id="password" name="password"
                                   value="${profile.password}" required
                                   placeholder="Input your current password">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="confirmPassword">Confirm password</label>
                            <input class="form-control" type="password" id="confirmPassword"
                                   name="confirmPassword" placeholder="Repeat a password" required>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="email">Birthday</label>
                            <input class="form-control" type="date" id="birthDay"
                                   name="birthDay"
                                   value="<fmt:formatDate type="date"
                                                           pattern="yyyy-MM-dd"
                                                           value="${profile.birthDay}"/>" required>
                        </div>
                    </div>


                    <div class="registrationText">
                        <label>
                            <input type="radio" name="gender" value="male" checked>

                            <div>Male</div>
                            <input type="radio" name="gender" value="female">

                            <div>Female</div>
                        </label>

                    </div>
                    <br><br><br><br>

                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="countries">Country</label>
                            <select class="form-control" id="countries" name="country">
                                <c:choose>
                                    <c:when test="${not empty profile.country}">
                                        <option value="${profile.country.countryId}"
                                                selected>${profile.country}</option>
                                        <option value="0">None selected</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="0" selected>---</option>
                                    </c:otherwise>
                                </c:choose>

                                <c:forEach items="${requestScope.countries}" var="country">
                                    <option value="${country.countryId}">${country.country}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="cities">City</label>
                            <select class="form-control" id="cities" name="city"
                                    value="${profile.city}">
                                <c:choose>
                                    <c:when test="${not empty profile.city}">
                                        <option value="${profile.city.cityId}"
                                                selected>${profile.city}</option>
                                        <option value="0">---</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="0" selected>---</option>
                                    </c:otherwise>
                                </c:choose>

                                <c:forEach items="${requestScope.cities}" var="city">
                                    <option value="${city.cityId}">${city.city}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>


                    <div class="text-center">
                        <button type="submit" class="btn btn-primary ">Sign In</button>
                    </div>
                </form>
                <div class="panel-footer " style="margin-top: 2px">
                    Back to <a href="login" onClick=""> login </a>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#countries").change(function () {
        console.log("a button was clicked");
        var countryId = $(this).val();
        $.get("cities/" + countryId, function (data) {
            $("#cities option").remove();
            $("#cities").append("<option value='0'>---</option>");
            for (var i = 0; i < data.length; i++) {
                $("#cities").append("<option value='" + data[i].cityId + "'>" + data[i].city + "</option>");
            }
        })
    });
</script>
</body>
</html>