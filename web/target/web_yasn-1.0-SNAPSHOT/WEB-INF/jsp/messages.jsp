<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>YASN</title>
    <link href="<c:url value="/static/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/static/css/yasn.css"/>" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
</head>
<body>
<div class="container" id="profile_page">
    <jsp:include page="header.jsp"></jsp:include>
    <div class="container-fluid">
        <div class="row">
            <jsp:include page="navigation.jsp"></jsp:include>
            <div class="col-md-10 main" id="content" style="border-left:1px solid #ccc">
                <div class='row text-center'>
                    <h4>You have ${fn:length(requestScope.interlocutors)} dialogs</h4>
                </div>
                <c:forEach var="message" items="${requestScope.interlocutors}">
                    <div class="row" style="margin-top: 30px">
                        <div class="col-md-2">
                            <a href="user?id=${message.userId}"><img
                                    class="img-responsive center-block"
                                    src="getImage/${message.userId}"
                                    alt="photo authorId = ${message.userId}"
                                    style="height:100px"></a>
                        </div>
                        <div class="col-md-10">
                            <a href="dialog?id=${message.userId}">
                                <div>
                                    <div class="row">
                                        <p class="text-primary">${message.firstName} ${message.lastName}</p>
                                    </div>
                                    <%--<div class="row">--%>
                                        <%--<p class="text-left">${interlocutor.text}</p>--%>
                                    <%--</div>--%>
                                    <%--<div class="row">--%>
                                        <%--<p class="text-info">--%>
                                            <%--<fmt:formatDate type="date"--%>
                                                            <%--pattern="dd MMM"--%>
                                                            <%--value="${interlocutor.date}"/> at--%>
                                            <%--<fmt:formatDate type="time"--%>
                                                            <%--pattern="h : m a"--%>
                                                            <%--value="${interlocutor.date}"/>--%>
                                        <%--</p>--%>
                                    <%--</div>--%>
                                </div>
                            </a>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</div>


<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>