<div class="col-md-2 sidebar">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 main" id="menu_left">
                <ul class="nav nav-sidebar">
                    <li><a href="user?id=${sessionScope.userId}">Profile</a></li>
                    <li><a href="#">Friends</a></li>
                    <li><a href="messages">Messages</a></li>
                    <li><a href="settings">Settings</a></li>
                    <li><a href="test">Angular</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>