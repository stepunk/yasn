<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%--<c:choose>--%>
    <%--<c:when test="${(not empty securityRestriction) and not securityRestriction['can view profile']}">--%>
        <%--<h2 class="error">You can't view profile</h2>--%>
    <%--</c:when>--%>
    <%--<c:otherwise>--%>

        <div class="row" style="border-top:1px solid #ccc; margin-top: 10px">
        <c:choose>
            <c:when test="${(not empty securityRestriction) and not securityRestriction['can post messages to wall']}">
            </c:when>
            <c:otherwise>

                    <div class="col-md-8 col-md-offset-2" style="margin-top: 10px;">
                        <form action="wall" method="post" role="form">
                            <div class="form-group" style="margin-bottom:3px ">
                                <label for="comment">Comment:</label>
                                <input type="hidden" id="usersWall" name="usersWall" value="${user.userId}">
                                <textarea class="form-control" rows="2" id="comment" name ="text"
                                          placeholder="start typing"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
        <c:forEach var="message" items="${requestScope.messages}">
            <div class="row" style="margin-top: 30px">
                <div class="col-md-2">
                    <a href="user?id=${message.authorId.userId}"><img
                            class="img-responsive center-block"
                            src="getImage/${message.authorId.userId}"
                            alt="photo authorId = ${message.authorId.userId}"
                            style="height:100px"></a>
                </div>
                <div class="col-md-10">
                    <div class="row">
                        <p class="text-primary">${message.authorId.firstName} ${message.authorId.lastName}</p>
                    </div>
                    <div class="row">
                        <p class="text-left">${message.text}</p>
                    </div>
                    <div class="row">
                        <p class="text-info">
                            <fmt:formatDate type="date"
                                            pattern="dd MMM"
                                            value="${message.date}"/> at
                            <fmt:formatDate type="time"
                                            pattern="h : m a"
                                            value="${message.date}"/>
                        </p>
                    </div>
                </div>
            </div>
        </c:forEach>
    <%--</c:otherwise>--%>
<%--</c:choose>--%>